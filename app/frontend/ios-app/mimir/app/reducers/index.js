import user from './user.reducer';
import news from './news.reducer';
import stocks from './stock.reducer';
import search from './search.reducer';
import navigation from './navigation.reducer';
import descriptions from './descriptions.reducer';

export {
  user,
  news,
  stocks,
  search,
  navigation,
  descriptions
}
