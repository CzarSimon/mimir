//User actions
export const RECIVE_USER = "RECIVE_USER";
export const FETCH_USER = "FETCH_USER";
export const CREATE_NEW_USER = "CREATE_NEW_USER";
export const ADD_TICKER = "ADD_TICKER";
export const REMOVE_TICKER = "REMOVE_TICKER";
export const TOGGLE_MODIFIABLE = "TOGGLE_MODIFIABLE";

//Logon actions
export const USER_LOGON = "USER_LOGON";

//Twitter data actions
export const RECIVE_TWITTER_DATA = "RECIVE_TWITTER_DATA";
export const FETCH_TWITTER_DATA = "FETCH_TWITTER_DATA"; //Add failure scenario

//Stock data actions
export const RECIVE_STOCK_DATA = "RECIVE_STOCK_DATA";
export const FETCH_STOCK_DATA = "FETCH_STOCK_DATA"; //Add failure scenario

export const RECIVE_UPDATED_STOCK_DATA = "RECIVE_UPDATED_STOCK_DATA";
export const UPDATE_STOCK_DATA = "UPDATE_STOCK_DATA"; //Add failure scenario

export const RECIVE_HISTORICAL_DATA = "RECIVE_HISTORICAL_DATA";
export const FETCH_HISTORICAL_DATA = "FETCH_HISTORICAL_DATA"; //Add failure scenario

//Navigation actions
export const SET_ACTIVE_TICKER = "SET_ACTIVE_TICKER";
export const SELECT_TAB = "SELECT_TAB";

//Company description actions
export const FETCH_COMPANY_DESC = "FETCH_COMPANY_DESC";
export const RECIVE_COMPANY_DESC = "RECIVE_COMPANY_DESC";
export const RECIVE_DESC_FAILURE = "RECIVE_DESC_FAILURE";

//Search actions
export const TOGGLE_SEARCH_ACTIVE = "TOGGLE_SEARCH_ACTIVE";
export const FETCH_SEARCH_RESULTS = "FETCH_SEARCH_RESULTS";
export const RECIVE_SEARCH_RESULTS = "RECIVE_SEARCH_RESULTS";

//News actions
export const FETCH_NEWS_ITEMS = "FETCH_NEWS_ITEMS";
export const RECIVE_NEWS_ITEMS = "RECIVE_NEWS_ITEMS";
export const RECIVE_NEWS_FAILURE = "RECIVE_NEWS_FAILURE;"
