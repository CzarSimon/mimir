# mimir 
![mimir-tiny-icon](https://cloud.githubusercontent.com/assets/9406331/15993573/7a8097b0-30ea-11e6-9375-70469828c217.png)

Stock news app that ranks the urgency the stocks in a of a users portfolio
based the Twitter activity around the stocks. Under development :)

The idea here is to use the twitter public streams to compare the volume of
tweets of individual stocks against the the average volume. This will be used
to give each stock an urgency score thereby giving the user an indication about
which stock to focus her attention on.

The second part is to provide the user with a list of the five (or some number)
of the most relevant news articles about each stock and thereby sparing the user
the trouble of searching for information themselves.

What will be built is a web scraper to gather news, a twitter scraper to get
some sense of the current public sentiment of a stock and finally a user
interface to interact with the information. The front-end will be created using
React-Native because I want to try it out :)

//Simon
