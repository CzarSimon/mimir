APP_SERVER = {
    "ADDRESS": "http://139.59.159.73",
    "routes": {
        "STOCKLIST": "/stockList"
    }
}

NEWS_SERVER = {
    "ADDRESS": "http://139.59.214.5:5000",
    "routes": {
        "RANK": "/rankArticle"
    }
}

forbidden_domains = ["owler.us", "owler.com", "stocktwits.com", "investorshangout.com", "1broker.com"]